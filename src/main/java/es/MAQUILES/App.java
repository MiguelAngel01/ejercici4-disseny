package es.MAQUILES;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class App extends Application{
	
	AnchorPane escena;
	
	public static void main(String[] args) {
		
		launch(args);
		
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		primaryStage.setTitle("Animacions i efectes");
		
        try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(App.class.getResource("Ejercicio4.fxml"));
			
			escena = loader.load();
			primaryStage.setScene(new Scene(escena));
			primaryStage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
