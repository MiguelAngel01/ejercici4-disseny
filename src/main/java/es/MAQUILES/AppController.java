package es.MAQUILES;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;

public class AppController implements Initializable{

	@FXML
	private Label vidasLabel;
	@FXML
	private Circle usuario;
	@FXML
	private AnchorPane scene;
	@FXML
	private Circle puntoA;
	@FXML
	private Circle puntoB;
	@FXML
	private Circle puntoC;
	
	private int vidas = 3;
	private double posX = 0;
	private double posY = 0;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		vidasLabel.setText("Vidas: " + vidas);
		usuario.setOnKeyPressed(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				
				TranslateTransition t;
				
				switch (event.getCode()) {
				case UP:
					usuario.setLayoutY(usuario.getLayoutY() - 5);
					break;
				case DOWN:
					usuario.setLayoutY(usuario.getLayoutY() + 5);
					break;
				case RIGHT:
					usuario.setLayoutX(usuario.getLayoutX() + 5);
					break;
				case LEFT:
					usuario.setLayoutX(usuario.getLayoutX() - 5);
					break;
				default:
					break;
				}
				
				tocarBorde();
				tocarCirculo();
				
			}
		});
		
	}
	
	private void tocarBorde() {
		
		if ((usuario.getLayoutX() + usuario.getRadius()) >= scene.getWidth() || (usuario.getLayoutX() - usuario.getRadius()) <= 0 ||
				(usuario.getLayoutY() + usuario.getRadius()) >= scene.getHeight() || (usuario.getLayoutY() - usuario.getRadius()) <= 0) {
			ScaleTransition sta = new ScaleTransition(Duration.seconds(1), usuario);
	        sta.setByX(-0.25);
	        sta.setByY(-0.25);

	        sta.play();
	        usuario.setLayoutX(200);
	        usuario.setLayoutY(200);
			vidas--;
		}
		
		vidasLabel.setText("Vidas: " + vidas);
		finalizar();
		
	}
	
	private void tocarCirculo() {
		
		if (((puntoA.getLayoutX() + puntoA.getRadius()) >= usuario.getLayoutX() && (puntoA.getLayoutX() - puntoA.getRadius()) <= usuario.getLayoutX()) &&
				((puntoA.getLayoutY() + puntoA.getRadius()) >= usuario.getLayoutY() && (puntoA.getLayoutY() - puntoA.getRadius()) <= usuario.getLayoutY())) {
			vidas++;
			puntoA.setLayoutX(Math.random() * 400);
			puntoA.setLayoutY(Math.random() * 400);
			ScaleTransition sta = new ScaleTransition(Duration.seconds(1), usuario);
	        sta.setByX(0.25);
	        sta.setByY(0.25);

	        sta.play();
		}
		
		if (((puntoB.getLayoutX() + puntoB.getRadius()) >= usuario.getLayoutX() && (puntoB.getLayoutX() - puntoB.getRadius()) <= usuario.getLayoutX()) &&
				((puntoB.getLayoutY() + puntoB.getRadius()) >= usuario.getLayoutY() && (puntoB.getLayoutY() - puntoB.getRadius()) <= usuario.getLayoutY())) {
			vidas++;
			puntoB.setLayoutX(Math.random() * 400);
			puntoB.setLayoutY(Math.random() * 400);
			ScaleTransition sta = new ScaleTransition(Duration.seconds(1), usuario);
	        sta.setByX(0.25);
	        sta.setByY(0.25);

	        sta.play();
		}
		
		if (((puntoC.getLayoutX() + puntoC.getRadius()) >= usuario.getLayoutX() && (puntoC.getLayoutX() - puntoC.getRadius()) <= usuario.getLayoutX()) &&
				((puntoC.getLayoutY() + puntoC.getRadius()) >= usuario.getLayoutY() && (puntoC.getLayoutY() - puntoC.getRadius()) <= usuario.getLayoutY())) {
			vidas++;
			puntoC.setLayoutX(Math.random() * 400);
			puntoC.setLayoutY(Math.random() * 400);
			ScaleTransition sta = new ScaleTransition(Duration.seconds(1), usuario);
	        sta.setByX(0.25);
	        sta.setByY(0.25);

	        sta.play();
		}
		
	}
	
	private void finalizar() {
		
		if (vidas <= 0) {
			ScaleTransition sta = new ScaleTransition(Duration.seconds(1), usuario);
	        sta.setByX(-0.5);
	        sta.setByY(-0.5);

	        sta.play();
	        
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Game Over");
			alert.setContentText("Te has quedado sin vidas");

			alert.showAndWait();
			Stage stage = (Stage) this.vidasLabel.getScene().getWindow();
	        stage.close();
		}
		
	}

}
