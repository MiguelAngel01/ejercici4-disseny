module es.MAQUILES {
    requires javafx.controls;
    requires javafx.fxml;
	requires javafx.graphics;

    opens es.MAQUILES to javafx.fxml;
    exports es.MAQUILES;
}